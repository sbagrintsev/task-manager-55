package ru.tsc.bagrintsev.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.tsc.bagrintsev.tm.dto.request.data.DataJacksonYamlSaveRequest;

@Component
public final class DataJacksonYamlSaveCommand extends AbstractDataCommand {

    @Override
    @SneakyThrows
    public void execute() {
        showOperationInfo();
        domainEndpoint.saveJacksonYaml(new DataJacksonYamlSaveRequest(getToken()));
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Save current application state in yaml file";
    }

    @NotNull
    @Override
    public String getName() {
        return "data-save-yaml";
    }

}
