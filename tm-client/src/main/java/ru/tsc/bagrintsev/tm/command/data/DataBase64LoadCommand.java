package ru.tsc.bagrintsev.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.tsc.bagrintsev.tm.dto.request.data.DataBase64LoadRequest;

@Component
public final class DataBase64LoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-load-base64";

    @Override
    @SneakyThrows
    public void execute() {
        showOperationInfo();
        domainEndpoint.loadBase64(new DataBase64LoadRequest(getToken()));
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Load current application state from base64 file";
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
