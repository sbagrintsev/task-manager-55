package ru.tsc.bagrintsev.tm.command.project;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.tsc.bagrintsev.tm.dto.request.project.ProjectChangeStatusByIdRequest;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.enumerated.Status;
import ru.tsc.bagrintsev.tm.util.TerminalUtil;

import java.util.Arrays;

@Component
public class ProjectChangeStatusByIdCommand extends AbstractProjectCommand {

    @Override
    @SneakyThrows
    public void execute() {
        showOperationInfo();
        showParameterInfo(EntityField.ID);
        @Nullable final String id = TerminalUtil.nextLine();
        showParameterInfo(EntityField.STATUS);
        System.out.println(Arrays.toString(Status.values()));
        @Nullable final String statusValue = TerminalUtil.nextLine();
        @NotNull final ProjectChangeStatusByIdRequest request = new ProjectChangeStatusByIdRequest(getToken());
        request.setId(id);
        request.setStatusValue(statusValue);
        projectEndpoint.changeProjectStatusById(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Change project status by id.";
    }

    @NotNull
    @Override
    public String getName() {
        return "project-change-status";
    }

}
