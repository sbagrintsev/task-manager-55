package ru.tsc.bagrintsev.tm.command.project;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.tsc.bagrintsev.tm.dto.model.ProjectDTO;
import ru.tsc.bagrintsev.tm.dto.request.project.ProjectListRequest;
import ru.tsc.bagrintsev.tm.dto.response.project.ProjectListResponse;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.enumerated.Sort;
import ru.tsc.bagrintsev.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

@Component
public class ProjectListCommand extends AbstractProjectCommand {

    @Override
    @SneakyThrows
    public void execute() {
        showOperationInfo();
        showParameterInfo(EntityField.SORT);
        System.out.println(Arrays.toString(Sort.values()));
        @Nullable final String sortValue = TerminalUtil.nextLine();
        @NotNull final ProjectListRequest request = new ProjectListRequest(getToken());
        request.setSortValue(sortValue);
        @Nullable final ProjectListResponse response = projectEndpoint.listProject(request);
        @Nullable final List<ProjectDTO> projects = response.getProjects();
        if (projects != null) {
            projects.forEach(System.out::println);
        }
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Print project list.";
    }

    @NotNull
    @Override
    public String getName() {
        return "project-list";
    }

}
