package ru.tsc.bagrintsev.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.tsc.bagrintsev.tm.dto.request.data.BackupSaveRequest;

@Component
public final class BackupSaveCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "backup-save";

    @Override
    @SneakyThrows
    public void execute() {
        domainEndpoint.saveBackup(new BackupSaveRequest(getToken()));
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Backup current application state in base64 file";
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
