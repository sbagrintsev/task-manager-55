package ru.tsc.bagrintsev.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.tsc.bagrintsev.tm.dto.request.data.DataJaxbXmlLoadRequest;

@Component
public final class DataJaxbXmlLoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-load-jaxb-xml";

    @Override
    @SneakyThrows
    public void execute() {
        showOperationInfo();
        domainEndpoint.loadJaxbXml(new DataJaxbXmlLoadRequest(getToken()));
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Load current application state from xml file";
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
