package ru.tsc.bagrintsev.tm.command.user;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.tsc.bagrintsev.tm.dto.request.user.UserSignOutRequest;
import ru.tsc.bagrintsev.tm.enumerated.Role;

@Component
public final class UserSignOutCommand extends AbstractUserCommand {

    @Override
    @SneakyThrows
    public void execute() {
        showOperationInfo();
        authEndpoint.signOut(new UserSignOutRequest(getToken()));
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Sign user out.";
    }

    @NotNull
    @Override
    public String getName() {
        return "sign-out";
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
