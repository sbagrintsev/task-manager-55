package ru.tsc.sbagrintsev.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Before;
import org.junit.experimental.categories.Category;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Controller;
import ru.tsc.bagrintsev.tm.api.sevice.IPropertyService;
import ru.tsc.bagrintsev.tm.api.sevice.dto.IProjectServiceDTO;
import ru.tsc.bagrintsev.tm.api.sevice.dto.ITaskServiceDTO;
import ru.tsc.bagrintsev.tm.api.sevice.dto.IUserServiceDTO;
import ru.tsc.bagrintsev.tm.configuration.ServerConfiguration;
import ru.tsc.bagrintsev.tm.dto.model.UserDTO;
import ru.tsc.bagrintsev.tm.exception.user.LoginAlreadyExistsException;
import ru.tsc.bagrintsev.tm.exception.user.LoginIsIncorrectException;
import ru.tsc.bagrintsev.tm.exception.user.PasswordIsIncorrectException;
import ru.tsc.sbagrintsev.tm.marker.DBCategory;

import java.security.GeneralSecurityException;
import java.util.Arrays;
import java.util.List;

@Controller
@Category(DBCategory.class)
public abstract class AbstractDTOTest {

    @NotNull
    public static final AnnotationConfigApplicationContext CONTEXT = new AnnotationConfigApplicationContext(ServerConfiguration.class);

    @NotNull
    protected IPropertyService propertyService = CONTEXT.getBean(IPropertyService.class);

    @NotNull
    protected ITaskServiceDTO taskService = CONTEXT.getBean(ITaskServiceDTO.class);

    @NotNull
    protected IProjectServiceDTO projectService = CONTEXT.getBean(IProjectServiceDTO.class);

    @NotNull
    protected IUserServiceDTO userService = CONTEXT.getBean(IUserServiceDTO.class);

    @After
    public void destroy() {
        taskService.clearAll();
        projectService.clearAll();
        userService.clearAll();
    }

    @Before
    public void init() throws LoginIsIncorrectException, GeneralSecurityException, LoginAlreadyExistsException, PasswordIsIncorrectException {
        @NotNull final UserDTO user1 = userService.create("test1", "testPassword1");
        user1.setId("testUserId1");
        @NotNull final UserDTO user2 = userService.create("test2", "testPassword2");
        user2.setId("testUserId2");
        @NotNull final List<UserDTO> list = Arrays.asList(user1, user2);
        userService.clearAll();
        userService.set(list);
    }

}
